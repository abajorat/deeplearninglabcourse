import pickle
import numpy as np
import random
import copy
from robo.fmin import bayesian_optimization
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

rf = pickle.load(open("./rf_surrogate_cnn.pkl", "rb"))
cost_rf = pickle.load(open("./rf_cost_surrogate_cnn.pkl", "rb"))


def objective_function(x, epoch=40):
    """
        Function wrapper to approximate the validation error of the
        hyperparameter configurations x by the prediction of a surrogate
        regression model,
        which was trained on the validation error of randomly sampled
        hyperparameter configurations.
        The original surrogate predicts the validation error after a given
        epoch. Since all hyperparameter configurations were trained for a
        total amount of 40 epochs, we will query the performance after epoch 40
    """

    # Normalize all hyperparameter to be in [0, 1]
    x_norm = copy.deepcopy(x)
    x_norm[0] = (x[0] - (-6)) / (0 - (-6))
    x_norm[1] = (x[1] - 32) / (512 - 32)
    x_norm[2] = (x[2] - 4) / (10 - 4)
    x_norm[3] = (x[3] - 4) / (10 - 4)
    x_norm[4] = (x[4] - 4) / (10 - 4)

    x_norm = np.append(x_norm, epoch)
    y = rf.predict(x_norm[None, :])[0]

    return y


def runtime(x, epoch=40):
    """
        Function wrapper to approximate the runtime of the hyperparameter
        configurations x.
    """

    # Normalize all hyperparameter to be in [0, 1]
    x_norm = copy.deepcopy(x)
    x_norm[0] = (x[0] - (-6)) / (0 - (-6))
    x_norm[1] = (x[1] - 32) / (512 - 32)
    x_norm[2] = (x[2] - 4) / (10 - 4)
    x_norm[3] = (x[3] - 4) / (10 - 4)
    x_norm[4] = (x[4] - 4) / (10 - 4)

    x_norm = np.append(x_norm, epoch)
    y = cost_rf.predict(x_norm[None, :])[0]

    return y


def random_search(incumbent) -> []:
    bestValue = 9999999999
    # best = []
    if incumbent:
        performance = []
    else:
        configurations = []
    for i in range(50):
        x1 = random.uniform(-6, 0)
        x2 = random.uniform(32, 512)
        x3 = random.uniform(4, 10)
        x4 = random.uniform(4, 10)
        x5 = random.uniform(4, 10)
        x = [x1, x2, x3, x4, x5]
        y = objective_function(x)
        if y < bestValue:
            bestValue = y
            # best = x
        if incumbent:
            performance.append(bestValue)
        else:
            configurations.append(x)
    # return performance
    if incumbent:
        return performance
    else:
        return configurations


def bayesian():
    lower = np.array([-6, 32, 4, 4, 4])
    upper = np.array([0, 512, 10, 10, 10])
    results = bayesian_optimization(objective_function, lower, upper,
                                    num_iterations=50)
    return results


def configBoth():
    x1 = np.array(random_search(False))
    x2 = bayesian()['X']
    pickle.dump(x1, open('randomTimeX.p', 'wb'))
    pickle.dump(x2, open('bayesianTimeX.p', 'wb'))


def incumbentBoth():
    # x1 = np.array(random_search(True))
    x2 = bayesian()['incumbent_values']
    for i in range(9):
        print(i)
        # x1 += np.array(random_search(True))
        x2 += bayesian()['incumbent_values']
    # x1 /= 10
    x2 = np.array(x2) / 10
    # pickle.dump(x1, open('random.p', 'wb'))
    pickle.dump(x2, open('bayesian.p', 'wb'))


def runtimeBoth():
    x1 = pickle.load(open('randomTimeX.p', 'rb'))
    x2 = pickle.load(open('bayesianTimeX.p', 'rb'))
    r1 = []
    r2 = []
    for i in range(50):
        if i == 0:
            r1.append(objective_function(x1[i]))
            r2.append(objective_function(x2[i]))
        else:
            r1.append(r1[i - 1] + objective_function(x1[i]))
            r2.append(r2[i - 1] + objective_function(x2[i]))
    return r1, r2


def plotRuntime():
    x = range(50)
    r1, r2 = runtimeBoth()
    plt.plot(x, r1, 'red')
    plt.plot(x, r2, 'green')
    red_patch = mpatches.Patch(color='red', label='Random Search')
    green_patch = mpatches.Patch(color='green', label='Bayesian Optimization')
    plt.legend(handles=[red_patch, green_patch])
    plt.ylabel('Cumulative Runtime')
    plt.xlabel('Iterations')
    plt.savefig('Runtime.png')
    # plt.show()

def plotPerformance():
    x = range(50)
    r1 = pickle.load(open('random.p', 'rb'))
    r2 = pickle.load(open('bayesian1.p', 'rb'))
    print(r2)
    plt.plot(x, r1, 'red')
    plt.plot(x, r2, 'green')
    red_patch = mpatches.Patch(color='red', label='Random Search')
    green_patch = mpatches.Patch(color='green', label='Bayesian Optimization')
    plt.legend(handles=[red_patch, green_patch])
    plt.ylabel('Performance')
    plt.xlabel('Iterations')
    plt.savefig('Performance.png')


if __name__ == "__main__":
    # r2 = pickle.load(open('bayesian.p', 'rb'))
    # print(r2)
    # incumbentBoth()
    # r1 = pickle.load(open('bayesian.p', 'rb'))
    # x1 = []
    # for i in range(50):
        # x1.append(0)
        # for j in range(10):
            # x1[i] += r1[50*j + i]
    # pickle.dump(x1, open('bayesian1.p', 'wb'))
    # plotPerformance()
    plotRuntime()
