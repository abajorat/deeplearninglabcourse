from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

# Imports
import tensorflow as tf
import numpy as np
import matplotlib.patches as mpatches
import _pickle as cPickle
import os
import gzip
import matplotlib.pyplot as plt
import h5py
import random

class Data:
  def __init__(self):
    with h5py.File("cell_data.h5", "r") as data:
      self.train_images = [data["/train_image_{}".format(i)][:] for i in range(28)]
      self.train_labels = [data["/train_label_{}".format(i)][:] for i in range(28)]
      self.test_images = [data["/test_image_{}".format(i)][:] for i in range(3)]
      self.test_labels = [data["/test_label_{}".format(i)][:] for i in range(3)]
    
    self.input_resolution = 300
    self.label_resolution = 116

    self.offset = (300 - 116) // 2

  def get_train_image_list_and_label_list(self):
    image_list = []
    label_list = []
    for i in range(50):
        n = random.randint(0, len(self.train_images) - 1)
        x = random.randint(0, (self.train_images[n].shape)[1] - self.input_resolution - 1)
        y = random.randint(0, (self.train_images[n].shape)[0] - self.input_resolution - 1)

        image = self.train_images[n][y:y + self.input_resolution, x:x + self.input_resolution, :]

        x += self.offset
        y += self.offset
        label = self.train_labels[n][y:y + self.label_resolution, x:x + self.label_resolution]
        image_list.append(image)
        label_list.append(label)
    return image_list, label_list

  def get_test_image_list_and_label_list(self):
    coord_list = [[0,0], [0, 116], [0, 232], 
                  [116,0], [116, 116], [116, 232],
                  [219,0], [219, 116], [219, 232]]
    
    image_list = []
    label_list = []
    
    for image_id in range(3):
      for y, x in coord_list:
        image = self.test_images[image_id][y:y + self.input_resolution, x:x + self.input_resolution, :]
        image_list.append(image)
        x += self.offset
        y += self.offset
        label = self.test_labels[image_id][y:y + self.label_resolution, x:x + self.label_resolution]
        label_list.append(label)
    

    return image_list, label_list
def autoencoder(features, labels, mode):
  """Model function for CNN."""
  # Input Layer
    
  input_layer = features["x"]
    
  # Convolutional Layer #1
  conv1 = tf.layers.conv2d(
      inputs=input_layer,
      filters=32,
      kernel_size=[3, 3],
      padding="valid", 
      activation=tf.nn.relu)
    
  conv2 = tf.layers.conv2d(
      inputs=conv1,
      filters=32,
      kernel_size=[3, 3],
      padding="valid", 
      activation=tf.nn.relu)
    
  # Pooling Layer #1
  pool1 = tf.layers.max_pooling2d(
      inputs=conv2,
      pool_size=[2, 2],
      strides=2)
  
  conv3 = tf.layers.conv2d(
      inputs=pool1,
      filters=64,
      kernel_size=[3, 3],
      padding="valid", 
      activation=tf.nn.relu)
    
  conv4 = tf.layers.conv2d(
      inputs=conv3,
      filters=64,
      kernel_size=[3, 3],
      padding="valid", 
      activation=tf.nn.relu)
    
  # Pooling Layer #1
  pool2 = tf.layers.max_pooling2d(
      inputs=conv4,
      pool_size=[2, 2],
      strides=2)
    
  conv5 = tf.layers.conv2d(
      inputs=pool2,
      filters=128,
      kernel_size=[3, 3],
      padding="valid", 
      activation=tf.nn.relu)
    
  conv6 = tf.layers.conv2d(
      inputs=conv5,
      filters=128,
      kernel_size=[3, 3],
      padding="valid", 
      activation=tf.nn.relu)
    
  # Pooling Layer #1
  pool3 = tf.layers.max_pooling2d(
      inputs=conv6,
      pool_size=[2, 2],
      strides=2)
    
  conv7 = tf.layers.conv2d(
      inputs=pool3,
      filters=256,
      kernel_size=[3, 3],
      padding="valid", 
      activation=tf.nn.relu)
    
  conv8 = tf.layers.conv2d(
      inputs=conv7,
      filters=256,
      kernel_size=[3, 3],
      padding="valid", 
      activation=tf.nn.relu)
    
  # Pooling Layer #1
  pool4 = tf.layers.max_pooling2d(
      inputs=conv8,
      pool_size=[2, 2],
      strides=2)
    
  conv9 = tf.layers.conv2d(
      inputs=pool4,
      filters=512,
      kernel_size=[3, 3],
      padding="valid", 
      activation=tf.nn.relu)
    
  conv10 = tf.layers.conv2d(
      inputs=conv9,
      filters=512,
      kernel_size=[3, 3],
      padding="valid", 
      activation=tf.nn.relu)
#   print(conv10.get_shape())

  trans1 = tf.layers.conv2d_transpose(
      inputs=conv10,
      filters=256,
      kernel_size=[2, 2],
      strides=2,
      padding="valid")
#   print(trans1.get_shape())

  conv11 = tf.layers.conv2d(
      inputs=tf.concat([conv8[0:,4:-4,4:-4],trans1], 3),
      filters=256,
      kernel_size=[3, 3],
      padding="valid", 
      activation=tf.nn.relu)

  conv12 = tf.layers.conv2d(
      inputs=conv11,
      filters=256,
      kernel_size=[3, 3],
      padding="valid", 
      activation=tf.nn.relu)
#   print(conv12.get_shape())
#   print(conv6.get_shape())

  trans2 = tf.layers.conv2d_transpose(
      inputs=conv12,
      filters=128,
      kernel_size=[2, 2],
      strides=2,
      padding="valid")
#   print(trans2.get_shape())
  conv13 = tf.layers.conv2d(
      inputs=tf.concat([conv6[0:,16:-16,16:-16],trans2], 3),
      filters=128,
      kernel_size=[3, 3],
      padding="valid", 
      activation=tf.nn.relu)
#   print(conv13.get_shape())

  conv14 = tf.layers.conv2d(
      inputs=conv13,
      filters=128,
      kernel_size=[3, 3],
      padding="valid", 
      activation=tf.nn.relu)
#   print(conv14.get_shape())
#   print(conv4.get_shape())

  trans3 = tf.layers.conv2d_transpose(
      inputs=conv14,
      filters=64,
      kernel_size=[2, 2],
      strides=2,
      padding="valid")
#   print(trans3.get_shape())
  conv15 = tf.layers.conv2d(
      inputs=tf.concat([conv4[0:,40:-40,40:-40],trans3], 3),
      filters=64,
      kernel_size=[3, 3],
      padding="valid", 
      activation=tf.nn.relu)
#   print(conv15.get_shape())

  conv16 = tf.layers.conv2d(
      inputs=conv15,
      filters=64,
      kernel_size=[3, 3],
      padding="valid", 
      activation=tf.nn.relu)
#   print(conv16.get_shape())
#   print(conv2.get_shape())

  trans4 = tf.layers.conv2d_transpose(
      inputs=conv16,
      filters=32,
      kernel_size=[2, 2],
      strides=2,
      padding="valid")
#   print(trans4.get_shape())
  conv17 = tf.layers.conv2d(
      inputs=tf.concat([conv2[0:,88:-88,88:-88],trans4], 3),
      filters=32,
      kernel_size=[3, 3],
      padding="valid", 
      activation=tf.nn.relu)
#   print(conv17.get_shape())

  conv18 = tf.layers.conv2d(
      inputs=conv17,
      filters=32,
      kernel_size=[3, 3],
      padding="valid", 
      activation=tf.nn.relu)
#   print(conv18.get_shape())

  output_layer = tf.layers.conv2d(
      inputs=conv18,
      filters=2,
      kernel_size=[1, 1],
      padding="valid")
 
  predictions = tf.argmax(output_layer, axis=3)
  if mode == tf.estimator.ModeKeys.PREDICT:
    return tf.estimator.EstimatorSpec(mode=mode, predictions=predictions)
#   if mode == tf.estimator.ModeKeys.PREDICT:
#     return tf.estimator.EstimatorSpec(mode=mode, predictions = tf.argmax(output_layer, axis=3))


  loss =  tf.reduce_sum(tf.nn.sparse_softmax_cross_entropy_with_logits(labels=labels, logits=output_layer)) / (116 * 116)
  accuracy = tf.metrics.mean_iou(labels, predictions, 2) 
#   Configure the Training Op (for TRAIN mode)
  if mode == tf.estimator.ModeKeys.TRAIN:
    optimizer = tf . train .AdamOptimizer(0.0001, 0.95, 0.99)
    train_op = optimizer.minimize(
        loss=loss,
        global_step=tf.train.get_global_step())
    return tf.estimator.EstimatorSpec(mode=mode, loss=loss, train_op=train_op)
    
    
    

#   Add evaluation metrics (for EVAL mode)
  
  eval_metric_ops = {"accuracy": accuracy}

  return tf.estimator.EstimatorSpec(
    mode=mode, loss=loss, eval_metric_ops=eval_metric_ops)



if __name__ == "__main__":
    tf.logging.set_verbosity(tf.logging.ERROR)
    data = Data()
    X_val, y_val = data.get_test_image_list_and_label_list()
    X_train, y_train = data.get_train_image_list_and_label_list()
    X_val = np.array(X_val).astype(np.float32)
    y_val = np.array(y_val).astype(np.int64)
    data_classifier = tf.estimator.Estimator(
        model_fn=autoencoder)
    tensors_to_log = {}
    logging_hook = tf.train.LoggingTensorHook(
        tensors=tensors_to_log, every_n_iter=50)

    plt.imsave("Image_0_val" + ".png", y_val[0])
    plt.imsave("Image_1_val" + ".png", y_val[1])
    plt.imsave("Image_2_val" + ".png", y_val[2])
    val_losses = []
    val_accs = []
    train_losses = []
    train_accs = []
    predic = None

    for i in range(1):
        print(i)
        X_train, y_train = data.get_train_image_list_and_label_list()
        X_train = np.array(X_train).astype(np.float32)
        y_train = np.array(y_train).astype(np.int64)
        train_input_fn = tf.estimator.inputs.numpy_input_fn(
            x={"x": X_train},
            y=y_train,
            batch_size=1,
            num_epochs=None,
            shuffle=True)
        data_classifier.train(
            input_fn=train_input_fn,
            steps=50,
            hooks=[logging_hook]
        )
        eval_input_fn = tf.estimator.inputs.numpy_input_fn(
            x={"x": X_val},
            y=y_val,
            num_epochs=1,
            shuffle=False
        )
        eval_results = data_classifier.evaluate(input_fn=eval_input_fn)
        eval_input_fn = tf.estimator.inputs.numpy_input_fn(
            x={"x": X_val[0:3]},
            y=y_val[0:3],
            num_epochs=1,
            shuffle=False
        )
        predictions = data_classifier.predict(input_fn=eval_input_fn)
        eval_input_fn = tf.estimator.inputs.numpy_input_fn(
            x={"x": X_train},
            y=y_train,
            num_epochs=1,
            shuffle=False
        )
        train_results = data_classifier.evaluate(input_fn=eval_input_fn)

        val_loss = eval_results['loss']    
        val_acc = eval_results['accuracy']
        train_loss =  train_results['loss']
        train_acc =  train_results['accuracy']
        train_losses.append(train_loss)
        train_accs.append(train_acc)
        val_losses.append(val_loss)
        val_accs.append(val_acc)
        print(train_loss)
        print(train_acc)
        print(val_loss)
        print(val_acc)
        print(eval_results)
        #if i==20 or  i ==400:
	#       for j, batch in enumerate(predictions):
        #            plt.imsave("Image_"+str(j)+"_i" + str(i) + ".png", batch)
    evals = [train_losses, train_accs, val_losses, val_accs]
    cPickle.dump(evals, open( "evals.p", "wb" ) )
    print("Fertig")
